'use client'
import React, { useState, useEffect } from 'react';
import { embedDashboard } from '@superset-ui/embedded-sdk';

const Dashboard = () => {
    const [guestToken, setGuestToken] = useState(null);

    useEffect(() => {
        const fetchAndSetGuestToken = async () => {
            const response = await fetch('/api/superset_guest_token');
            const data = await response.json();
            setGuestToken(data.token);
        };

        fetchAndSetGuestToken();
    }, []);

    useEffect(() => {
        if (guestToken) {

            embedDashboard({
                id: "dashboard-id",
                supersetDomain: "https://your-superset-domain.com",
                mountPoint: document.getElementById("superset-container"),
                fetchGuestToken: () => Promise.resolve(guestToken),
                // dashboardUiConfig: (optional)
            });
        }
    }, [guestToken]);

    return (
        <div id="superset-container" style={{width:'100%', height:'600px'}}></div>
    );
};

export default Dashboard;
