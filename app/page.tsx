'use client'
import { useEffect, useState } from "react";
import Typist from "react-typist-component";



const Login = () => {
    return (
        <div className="login-container">
            <img src="/iaf_logo.png" alt="Left Image" className="center-image"/>
            <form>
                <div className="form-group">
                    <input type="text" id="username" name="username" placeholder="Username" />
                </div>
                <div className="form-group">
                    <input type="password" id="password" name="password" placeholder="Password" />
                </div>
                <button type="submit">Sign In</button>
            </form>
        </div>
    );
};


const VideoBackground = () => {
    const [showLogin, setShowLogin] = useState(false);

    useEffect(() => {
        const timer = setTimeout(() => {
            setShowLogin(true);
        }, 15000);

        return () => clearTimeout(timer);
    }, []);

    return (
        <div className='video-container'>
            <video autoPlay loop muted className='video'>
                <source src="/test.mp4" type="video/mp4"/>
            </video>
            <img src="/1.webp" alt="Your image description" className='image'/>
            <div className="terminal-box">
                <div className='terminal'>  
                    <div className="terminal-content line-1 anim-typewriter">
                        <h1 className="text1 animated-text">OP-STAMINA</h1>
                        {/* <p className="text3">वायु उत्कृष्टतां पुनर्निर्धारयन्ती</p> */}
                        {/* <h5 className="text2">Redefining Air Superiority: Indian Air Force Elevates with AI-Driven Innovation</h5> */}
                        <p className="text2">...वायु उत्कृष्टतां पुनर्निर्धारयन्ती</p>
                        
                    </div>
                    {/* <div className="images-container">
                        <img src="/1.webp" alt="Left Image" className="left-image"/>
                        <img src="/2.webp" alt="Right Image" className="right-image"/>
                    </div> */}
                </div>
            </div>

            {showLogin ? (
                <div className="login-box">
                    <Login />
                </div>
            ) : (
                <div className='commandPrompt'>  {/* Command prompt container */}
                    <div className="window-header">
                        <div className="window-header-buttons">
                            <div className="window-header-button"></div>
                            <div className="window-header-button"></div>
                            <div className="window-header-button"></div>
                        </div>
                        <div className="window-title">Command Prompt</div>
                    </div>
                    <Typist typingDelay={100} cursor={<span className='cursor'>|</span>}>
                        Welcome to OP Stamina...
                        <br />
                        Orchestrating Precision...... 
                        <br />
                        Surpassing Targets ..... 
                        <br />
                        Turbocharged by AI ..... 
                        <br />
                        Mastering Air Supremacy .....
                    </Typist>
                </div>
            )}
        </div>
    );
};

export default function Home() {
    return (

        <VideoBackground />
    );
}
